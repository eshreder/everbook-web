define(function(require) {
    'use strict';
    var ko = require('knockout');
    var templateMarkup = require('text!./inapp-google.html');
    var ds = require('datastorage');

    function Controller() {
        var self = this;
        self.selectLocalFile = ds.selectLocalFile;
    }

    Controller.prototype.dispose = function() { };
    return { viewModel: Controller, template: templateMarkup };
});