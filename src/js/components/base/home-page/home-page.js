define(function(require) {
    'use strict';
    var templateMarkup = require('text!./home-page.html');

    function Controller(){
        l.log('Controller HomePage Init');
    }

    Controller.prototype.dispose = function() { };
    return { viewModel: Controller, template: templateMarkup };
});