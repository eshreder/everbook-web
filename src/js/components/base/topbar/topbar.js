define(function(require) {
    'use strict';
    var ko = require('knockout');
    var templateMarkup = require('text!./topbar.html');
    var user = require('user');
    function Controller(params) {
        var self = this;
        self.user = user.user;
        self.logout = logout;

        function logout() {
            user.logout().then(function() {
               document.location.href = '/';
            });
        }
    }

    Controller.prototype.dispose = function() { };
    return { viewModel: Controller, template: templateMarkup };
});