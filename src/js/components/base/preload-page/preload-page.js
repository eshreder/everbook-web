define(function(require) {
    'use strict';
    var templateMarkup = require('text!./preload-page.html');

    function Controller(){
        console.log('Controller preload Init');
    }

    Controller.prototype.dispose = function() { };
    return { viewModel: Controller, template: templateMarkup };
});