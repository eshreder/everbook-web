define(function(require) {
    'use strict';
    var ko = require('knockout');
    var templateMarkup = require('text!./landing-page.html');
    var config = require('json!configs/config.json');
    var router = require('router');
    var dc = require('datacontext');
    var user = require('user');

    function Controller(){
        var self = this;
        self.login = ko.observable();
        self.password = ko.observable();
        self.send = send;

        function send() {
            user.login({
                login: self.login(),
                password: self.password()
            }).then(function(d) {
                router.openLink('main', 'main');
            }).catch(function(d) {});
        }
    }

    Controller.prototype.dispose = function() { };
    return { viewModel: Controller, template: templateMarkup };
});