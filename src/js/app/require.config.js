var require = {
    baseUrl: '/js/',
    paths: {
        'knockout':             '../bower_modules/knockout/dist/knockout',
        'jquery':               '../bower_modules/jquery/dist/jquery.min',
        'jquery.easing':        '../bower_modules/jquery.easing/js/jquery.easing.min',
        'crossroads':           '../bower_modules/crossroads/dist/crossroads.min',
        'signals':              '../bower_modules/signals/dist/signals.min',
        'historyjs':            '../bower_modules/history.js/scripts/bundled/html4+html5/native.history',
        'text':                 '../bower_modules/requirejs-text/text',
        'json':                 '../bower_modules/requirejs-plugins/src/json',
        'es5-shim':             '../bower_modules/es5-shim/es5-shim.min',
        'es5-sham':             '../bower_modules/es5-shim/es5-sham.min',
        'es6-shim':             '../bower_modules/es6-shim/es6-shim.min',
        'css':                  '../bower_modules/require-css/css.min',
        'rrule':                '../bower_modules/rrule/lib/rrule',
        'limit':                '../bower_modules/limit/limit',
        'materialize':          '../bower_modules/materialize/js/global',
        'materialize.forms':          '../bower_modules/materialize/js/forms',
        'materialize.buttons':          '../bower_modules/materialize/js/buttons',
        'materialize.dropdown':          '../bower_modules/materialize/js/dropdown',
        'jquery-upload':        '../bower_modules/jquery-file-upload/js/jquery.fileupload',
        'jquery.ui.widget':     '../bower_modules/jquery-file-upload/js/vendor/jquery.ui.widget',
        'jquery.iframe-transport': '../bower_modules/jquery-file-upload/js/jquery.iframe-transport',
        'custom-binding':       'app/custom-binding',
        'custom-extenders':     'app/custom-extenders',
        'l10n':                 'app/l10n',
        'logger':               'app/logger',
        'router':               'app/router',
        'user':                 'app/user',
        'oauth':                'app/oauth',
        'datacontext':          'app/data-context',
        'datastorage':          'app/datastorage',
        'progressbar':          'app/progressbar'
    },
    shim: {
        'materialize.forms': {deps: ['materialize']},
        'materialize.buttons': {deps: ['materialize']},
        'materialize.dropdown': {deps: ['materialize', 'jquery.easing']},
        'materialize': {deps: ['jquery']},
        'jquery.easing': {deps: ['jquery']},
        'jquery-upload': {deps: ['jquery.iframe-transport']},
        'historyjs': {
            exports: 'History'
        }
    }
};
