define(function(require) {
    'use strict';
    var ko = require('knockout');
    ko.extenders.number = function(target, option) {
        target.subscribe(function (newValue) {
            var cleanValue = +(newValue.toString().replace(/[^\d]/, ''));
            var min = option.min || 0;
            var max = option.max || 100;
            target(Math.min(Math.max(min, cleanValue), max));
        });
        return target;
    };
    ko.extenders.time = function (target, option) {
        target.subscribe(function (newValue) {
            var cleanValue = newValue.replace(/[^\d]/g, '').slice(0, 4);
            var match = cleanValue.match(/([\d]{0,2})([\d]{2})$/);
            if (!match) {
                match = [0, 0, cleanValue];
            }
            match[1] = +match[1];
            match[2] = +match[2];
            var tC = Math.floor(match[2] / 60);
            match[2] = match[2] % 60;
            match[1] += tC;
            if (option.maxDay) {
                match[1] = match[1] > 23 ? 23 : match[1];
            }
            if (match[1] || cleanValue.length > 2) {
                target(match[1] + ':' + (match[2] < 10 ? '0' : '') + match[2]);
            } else {
                target(cleanValue);
            }
            if (option.parsed) {
                option.parsed([+match[1], +match[2]]);
            }
        });
        return target;
    };
});