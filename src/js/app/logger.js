define(function(require) {
    'use strict';
    var config = require('json!configs/config.json');
    var exports = {
        log: log
    };

    function log() {
        if (config.debug) {
            var c = [+(new Date())];
            for (var i in arguments) {
                c.push(arguments[i]);
            }

            console.log.apply(console,c)
        }
    }
    return exports;
});