/*
* Работа с пользователем
* */
define(function(require) {
    'use strict';
    var dc = require('datacontext');
    var ko = require('knockout');
    function User() {
        var self = this;
        var user = self.user = ko.observable(null);
        self.getLogin = getLogin;
        self.logout = logout;
        self.login = login;

        function getLogin() {
            return dc.api.getLogin({}).then(function(d) {
                user(d);
            });
        }
        function login(data) {
            return dc.api.login({}, data).then(function(d) {
                user(d);
            });
        }
        function logout() {
            return dc.api.logout({}).then(function() {
                user(null);
            });
        }
    }

    return new User();
});