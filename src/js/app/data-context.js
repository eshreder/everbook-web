/*
* Работа с запросами
* */
define(function(require) {
    'use strict';

    var config = require('json!configs/config.json');
    var ko = require('knockout');
    var $ = require('jquery');
    var methods = require('json!configs/api-methods.json');
    var progressbar = require('progressbar');

    var ajaxSettings = {
        dataType: 'json',
        xhrFields:{withCredentials: true}
    };

    function sendRequest(path, type, data) {
        var xhr, promise = new Promise(
            function(resolve, reject) {
                progressbar.show();
                xhr = $.ajax($.extend({}, ajaxSettings, {
                    url: config.apiHost + path,
                    type: type,
                    data: data,
                    success: function(data) {
                        resolve(data);
                    },
                    error: function(jqXHR, status) {
                        var response;
                        if(status === 'abort'){
                            response = {
                                'error': {
                                    type: 'abort',
                                    code: 0,
                                    message: ''
                                }
                            };
                        }
                        else{
                            try {
                                response = JSON.parse(jqXHR.responseText);
                            }
                            catch (e) {
                                response = {'error': {
                                    code: jqXHR.status,
                                    message: ''
                                }};
                            }
                        }
                        reject(response);
                    },
                    complete: function() {
                        progressbar.hide();
                    }
                }));
            }
        );
        promise._abort = function(){
            xhr.abort();
        };
        return promise;
    }

    function DataContext() {
        var self = this;
        var api = self.api = {};

        ko.utils.objectForEach(methods, function(methodName, method) {
            var chunks = method.path.split(/(\{[^\}]+})/);
            api[methodName] = function(routeParams, data) {
                var path = '';
                for (var i = 0; i < chunks.length; i++) {
                    if (chunks[i].charAt(0) === '{') {
                        path += routeParams[chunks[i].substring(1, chunks[i].length - 1)];
                    } else {
                        path += chunks[i];
                    }
                }
                return sendRequest(path, method.type, data);
            };
        });
        self.setParam = setParam;
        self.getParam = getParam;

        function setParam(key, value) {
            if (typeof(Storage) !== 'undefined') {
                return localStorage.setItem(key, value);
            } else {
                //TODO: cookie_storage;
            }
        }
        function getParam(key) {
            if (typeof(Storage) !== 'undefined') {
                return localStorage.getItem(key);
            } else {
                //TODO: cookie_storage;
            }
        }
    }
    return new DataContext();
});
