define(function(require) {
    'use strict';

    var ko = require('knockout');
    var $ = require('jquery');
    var crossroads = require('crossroads');
    var history = require('historyjs');
    var routes = require('json!configs/routes.json');
    var l = require('logger');

    function getLocation(href) {
        var location = document.createElement('a');
        location.href = href;
        if (location.host == '') {
            location.href = location.href;
        }
        return location;
    }

    function Router(routes) {
        var self = this;
        var currentRoute = self.currentRoute = ko.observable({});
        var currentPage = self.currentPage = ko.observable();

        ko.utils.arrayForEach(routes, function(route) {
            crossroads.addRoute(route.path, function(requestParams) {
                currentRoute(ko.utils.extend(requestParams, route));
                currentPage(route.page);
            });
        });

        self.showTopbar = ko.computed(function() {
            return currentPage() && (currentPage() !== 'landing-page');
        });
        self.openLink = function(href, title){
            l.log([href, title]);
            return history.pushState({urlPath: href}, href, title);
        };

        $('body').on('click', 'a[data-link]', function(e) {
            e.preventDefault();
            return history.pushState({urlPath: this.href}, this.href, this.title);
        });

        history.Adapter.bind(window, 'statechange', function() {
            var State = history.getState();

            if (State.data.urlPath) {
                return crossroads.parse(getLocation(State.data.urlPath).pathname);
            }
            else {
                if (State.hash.length > 1) {
                    return crossroads.parse(State.hash);
                }
            }
        });

        crossroads.normalizeFn = crossroads.NORM_AS_OBJECT;
        crossroads.parse(getLocation(history.getPageUrl()).pathname);
    }
    return new Router(routes);
});
