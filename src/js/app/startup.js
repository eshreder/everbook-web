define(function(require) {
    'use strict';
    var ko = require('knockout');
    var components = require('json!configs/components.json');
    var constants = require('json!configs/constants.json');
    //var es5 = require('es5-shim');
    //var es5Sham = require('es5-sham');
    //var es6 = require('es6-shim');
    var router = require('router');
    var user = require('user');
    var l = require('logger');
    require('custom-binding');
    require('custom-extenders');
    require('limit');

    require('materialize.forms');
    require('materialize.buttons');
    require('materialize.dropdown');

    Promise.prototype.abort = function(){
        if(typeof this._abort === 'function'){
            this._abort();
        }
    };
    Promise.prototype.__then = Promise.prototype.then;
    Promise.prototype.then = function(){
        var result = Promise.prototype.__then.apply(this, arguments);
        if(typeof this._abort === 'function'){
            result._abort = this._abort;
        }
        return result;
    };
    Promise.prototype.__catch = Promise.prototype.catch;
    Promise.prototype.catch = function(){
        var result = Promise.prototype.__catch.apply(this, arguments);
        if(typeof this._abort === 'function'){
            result._abort = this._abort;
        }
        return result;
    };

    window.l = l;
    window.__EVB_CONSTANTS = constants;
    for (var i = 0; i < components.length; i++) {
        ko.components.register(components[i].name, { require: components[i].path });
    }

    user.getLogin().then(function() {
        if (['preload-page','landing-page'].indexOf(router.currentPage()) !== -1){
            router.openLink('main', 'main');
        }
    }).catch(function() {
        router.openLink('welcome', 'welcome');
    });

    ko.applyBindings({ router:router });
});
