define(function(require) {
    'use strict';

    var ko = require('knockout');
    var translations = require('json!configs/translations.json');
    //var translit = require('json!/config/translit.json');
    var dc = require('datacontext');

    var exports = {};

    var locales = exports.locales = ko.observableArray([
        {title:'РУС', key:'ru'},
        {title:'ENG', key:'en'},
        {title:'УКР', key:'uk'},
        {title:'TUR', key:'tu'}
    ]);
    var locale = exports.locale = ko.observable(null);
    var savedLocal = dc.getParam('local');
    if (savedLocal) {
        locales().some(function(l) {
            return l.key === savedLocal && locale(l);
        });
    }
    if (!locale()) {
        locale(locales()[0]);
    }
    exports.titleActiveLang = ko.computed(function() {
        return locale().title;
    });
    exports.switchLang = function(newLocale) {
        locale(newLocale);
        dc.setParam('local', locale().key);
    };
    exports.getTranslit = function(str) {
        var out = '';
        for (var i = 0; i < str.length; i++) {
            out += translit[str[i]] || '';
        }
        return out;
    };
    exports.getTranslation = function(key, params) {
        var localString = translations[locale().key][key];

        if (params) {
            for (var param in params) {
                if (params.hasOwnProperty(param)) {
                    localString = localString.replace(new RegExp('{' + param + '}', 'g'), params[param]);
                }
            }
        }
        return localString;
    };
    /* Локализация для числительных. в зависимости от value отдает key_(0|1|2)*/
    exports.getTranslationNum = function(key, value) {
        var cases = [2, 0, 1, 1, 1, 2];
        var indexNum = (value % 100 > 4 && value % 100 < 20) ? 2 : cases[(value % 10 < 5) ? value % 10 : 5];
        return exports.getTranslation(key + '_' + indexNum);
    };

    window.l10n = exports;
    return exports;
});
