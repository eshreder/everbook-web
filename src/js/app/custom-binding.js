define(function(require) {
    'use strict';
    var ko = require('knockout');
    var $ = require('jquery');
    var l10n = require('l10n');
    ko.bindingHandlers.autoScroll = {
        update: function(element, valueAccessor) {
            var $el = $(element);
            var val = ko.unwrap(valueAccessor());
            var sEl = $el.find(val.selectorElement).eq(val.trigger);
            if (sEl.length > 0) {
                $el.scrollTop(sEl[0].offsetTop + sEl.height() - $el.height());
            }
        }
    };
    ko.bindingHandlers.numberInput = {
        init: function(element, valueaccessor) {
            var $element = $(element);
            var obj = valueaccessor();
            $element.bind('click', '.minus, .plus', function(e) {
                if ($(e.target).is('.minus')) {
                    obj(obj() - 1);
                } else if ($(e.target).is('.plus')){
                    obj(obj() + 1);
                }
            });
        }
    };
    ko.bindingHandlers.cssFocus = {
        init: function(element) {
            $(element).bind('focusin', function(){
                $(element).addClass('is-focused');
            }).bind('focusout', function(){
                $(element).removeClass('is-focused');
            })
        }
    };
    ko.bindingHandlers.inputFocus = {
        init: function(element) {
            var $element = $(element);
            $element.bind('click', function(e){
                e.preventDefault();
                $element.select();
            });
        }
    };
    ko.bindingHandlers.translation = {
        update: function(element, valueAccessor) {
            var val = ko.unwrap(valueAccessor());
            var key;
            var params;
            var text;
            if (typeof val === 'string' || val instanceof String) {
                key = val;
            } else {
                key = val.key;
                params = val.params;
            }
            if (params && params.number) {
                text = l10n.getTranslationNum(key, params.number);
            } else {
                text = l10n.getTranslation(key);
            }
            if (element.tagName.toUpperCase() === 'INPUT') {
                if (params && params.placeholder) {
                    element.placeholder = text;
                } else {
                    element.value = text;
                }
            } else {
                element.innerHTML = text;
            }
        }
    };
    ko.bindingHandlers.dropdown = {
        init: function(element) {
            $(element).dropdown();
        }
    }
});
