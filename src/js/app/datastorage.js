define(function(require) {
    'use strict';
    var config = require('json!configs/config.json');
    var dc = require('datacontext');
    var $ = require('jquery');
    var progressbar = require('progressbar');
    require('jquery-upload');

    function DataStorage() {
        var self = this;
        var baseUrl = config.storageHost;
        var slowUpload = [baseUrl, 'api', 'files', 'upload'].join('/');
        var inc = 0;
        self.uploadFile = uploadFile;
        self.selectLocalFile = selectLocalFile;

        function selectLocalFile() {
            var $input = $('<input class="datastorage" name="file" type="file"/>');
            $('.datastorage[type="file"]').remove();
            $input.attr('id', 'file_' + (inc++)).css({
                position: 'absolute',
                top: '-1px',
                left: '-1px',
                width: '1px',
                height: '1px',
                opacity: '0px'
            }).appendTo('body');
            $input.bind('change', function(e) {
                progressbar.show();
                uploadFile(e.target.files).then(progressbar.hide);
            });
            $input.click();
        }
        function uploadFile(fileList) {
            return __getToken().then(upload.bind({}, fileList, {}));
            //var pUpload = new Promise(function(resolve, reject) {
            //    __getToken().then(function(d) {
            //        __upload(d.token, fileInput).then(resolve).catch(reject);
            //    }).catch(reject);
            //});
            //return pUpload;
        }
        function upload(fileInput, opt, token) {
            return __upload(token, fileInput);
        }
        function __upload(token, fileInput, progress) {
            var xhr, pUpload = new Promise(function(resolve, reject) {
                var _fileInput = $('<input type="file" name="file">');
                _fileInput.fileupload({
                    autoUpload: false,
                    formData: token
                });
                if(typeof progress === 'function'){
                    _fileInput.on('fileuploadprogress', function(e, data){
                        progress(data);
                    });
                }
                _fileInput.fileupload('option', {
                    xhrFields: {
                        withCredentials: true
                    },
                    url: slowUpload
                });
                xhr = _fileInput.fileupload('send', {
                    files: fileInput
                }).success(function(result) {
                    resolve(result);
                }).error(function(jqXHR, textStatus, errorThrown) {
                    reject({
                        xhr: jqXHR,
                        status: textStatus,
                        errorThrown: errorThrown
                    });
                });
            });
            pUpload._abort = function(){
                xhr.abort();
            };
            return pUpload;
        }
        function __getToken() {
            return dc.api.getTokenFile({});
        }
    }
    return new DataStorage();
});