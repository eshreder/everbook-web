define(function(require) {
    'use strict';
    var ko = require('knockout');
    var $ = require('jquery');

    function ProgressBar() {
        var self = this;
        var tmpl = '<div class="progress">' +
                        '<div class="indeterminate"></div>' +
                    '</div>';
        var qu = ko.observable(0);
        var obj;
        self.show = show;
        self.hide = hide;
        function show() {
            qu(qu()+ 1);
            if (!obj) {
                obj = $(tmpl);
                obj.appendTo('body');
            }
        }
        function hide() {
            qu(qu() - 1)
            if (obj && qu() === 0) {
                obj.remove();
                obj = undefined;
            }
        }
    }

    return new ProgressBar();
});